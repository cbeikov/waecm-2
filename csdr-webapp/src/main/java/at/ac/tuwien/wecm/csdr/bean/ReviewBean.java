package at.ac.tuwien.wecm.csdr.bean;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import at.ac.tuwien.wecm.csdr.data.TranslationDataAccess;
import at.ac.tuwien.wecm.csdr.model.Translation;
import at.ac.tuwien.wecm.csdr.model.TranslationReview;
import at.ac.tuwien.wecm.csdr.service.TranslationService;


/**
 * controller for Review
 */
@Named
@SessionScoped
public class ReviewBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private TranslationDataAccess translationDataAccess;
	@Inject
	private TranslationService translationService;
	@Inject
	private UserBean userBean;
	
	private String fromLanguage;
	private String toLanguage;
	
	private Translation translation;
	private String originalText;
	private String correctedLanguage;
	private String translatedText;
	private boolean correct = true;
	private String reviewComment;
	
	public String startReview(String fromLanguage, String toLanguage) {
		this.fromLanguage = fromLanguage;
		this.toLanguage = toLanguage;

		return nextTranslation();
	}
	
	/**
	 * marks translation as Spam and gets next translation
	 * @return
	 */
	public String spamTranslation() {
		TranslationReview review = new TranslationReview();
		review.setMarkedAsSpam(true);
		review.setRelatedTranslation(translation);
		review.setUser(userBean.getUsername());
		translationService.addReview(review);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Text wurde als Spam markiert!", ""));
		return nextTranslation();
	}
	
	/**
	 * saves current review and gets next translation
	 * @return
	 */
	public String saveReview() {
		TranslationReview review = new TranslationReview();
		
		if (!fromLanguage.equals(correctedLanguage)) {
			review.setCorrectedLanguage(correctedLanguage);
		}

		if (!correct) {
			review.setReviewComment(reviewComment);
		}
		
		review.setRelatedTranslation(translation);
		review.setUser(userBean.getUsername());
		translationService.addReview(review);
		return nextTranslation();
	}
	
	/**
	 * skips the current translation and gets next translation
	 * @return
	 */
	public String skipReview() {
		TranslationReview review = new TranslationReview();
		
		if (!fromLanguage.equals(correctedLanguage)) {
			review.setCorrectedLanguage(correctedLanguage);
		}
		
		review.setMarkedAsSkipped(true);
		review.setRelatedTranslation(translation);
		review.setUser(userBean.getUsername());
		translationService.addReview(review);
		return nextTranslation();
	}
	
	private String nextTranslation() {
		translation = translationDataAccess.getNextTranslation(userBean.getUsername(), fromLanguage, toLanguage);
		
		if (translation == null) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Momentan sind keine Texte für die gewählten Sprachen verfügbar!", ""));
			return "translation.xhtml";
		} else {
			originalText = translation.getRelatedText().getOriginalText();
			correctedLanguage = fromLanguage;
			translatedText = translation.getTranslatedText();
			correct = true;
			reviewComment = null;
			return "doReview.xhtml";
		}
	}
	
	public String getFromLanguage() {
		return fromLanguage;
	}
	public void setFromLanguage(String fromLanguage) {
		this.fromLanguage = fromLanguage;
	}
	public String getToLanguage() {
		return toLanguage;
	}
	public void setToLanguage(String toLanguage) {
		this.toLanguage = toLanguage;
	}
	public String getCorrectedLanguage() {
		return correctedLanguage;
	}
	public void setCorrectedLanguage(String correctedLanguage) {
		this.correctedLanguage = correctedLanguage;
	}
	public String getOriginalText() {
		return originalText;
	}
	public void setOriginalText(String originalText) {
		this.originalText = originalText;
	}
	public String getTranslatedText() {
		return translatedText;
	}
	public void setTranslatedText(String translatedText) {
		this.translatedText = translatedText;
	}

	public boolean isCorrect() {
		return correct;
	}

	public void setCorrect(boolean correct) {
		this.correct = correct;
	}

	public String getReviewComment() {
		return reviewComment;
	}

	public void setReviewComment(String reviewComment) {
		this.reviewComment = reviewComment;
	}
}
