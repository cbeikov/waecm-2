package at.ac.tuwien.wecm.csdr.bean;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Named
@ViewScoped
public class TranslationViewBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String fromLanguage;
	private String toLanguage;
	@NotNull
	@Size(min = 1)
	public String getFromLanguage() {
		return fromLanguage;
	}
	public void setFromLanguage(String fromLanguage) {
		this.fromLanguage = fromLanguage;
	}
	@NotNull
	@Size(min = 1)
	public String getToLanguage() {
		return toLanguage;
	}
	public void setToLanguage(String toLanguage) {
		this.toLanguage = toLanguage;
	}
}
