package at.ac.tuwien.wecm.csdr.service;

import java.util.UUID;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import at.ac.tuwien.wecm.csdr.data.TranslationDataAccess;
import at.ac.tuwien.wecm.csdr.model.TextToTranslate;
import at.ac.tuwien.wecm.csdr.model.Translation;
import at.ac.tuwien.wecm.csdr.model.TranslationReview;

@Stateless
public class TranslationService {
	
	@PersistenceContext
	private EntityManager em;
	@Inject
	private TranslationDataAccess translationDataAccess;

	/**
	 * persists text (to be translated)
	 * @param text
	 * @return
	 */
	public UUID addText(TextToTranslate text) {
		UUID id = UUID.randomUUID();
		text.setExternalId(id);
		em.persist(text);
		em.flush();
		return id;
	}
	
	/**
	 * persists the translation
	 * @param translation
	 */
	public void addTranslation(Translation translation) {
		if (translation.getCorrectedLanguage() != null) {
			TextToTranslate text = em.find(TextToTranslate.class, translation.getRelatedText().getId());
			text.setChoosenLanguage(translation.getCorrectedLanguage());
			text = em.merge(text);
			translation.setRelatedText(text);
		}
		
		em.persist(translation);
		em.flush();
		
		if (translation.isMarkedAsSpam() && translationDataAccess.getSpamCount(translation.getRelatedText().getId()) > 1) {
			System.out.println("We found spam!! " + em.find(TextToTranslate.class, translation.getRelatedText().getId()));
		}
	}
	
	/**
	 * persists the review <p>
	 * 
	 * if there are 2 "incorrect" reviews -> translation gets marked as wrong <p>
	 * if there are 2 "correct" reviews -> translation gets accepted <p>
	 * if marked 2 times as Spam -> informs the callback about the Spam
	 * @param review
	 */
	public void addReview(TranslationReview review) {
		em.persist(review);
		em.flush();
		
		if (review.getReviewComment() != null && translationDataAccess.getBadReviewCount(review.getRelatedTranslation().getId()) > 1) {
			// 2 times incorrect review -> this translation gets marked as wrong
			review.getRelatedTranslation().setMarkedAsWrong(true);
			em.merge(review.getRelatedTranslation());
		} else if (review.isMarkedAsSpam() && translationDataAccess.getSpamCount(review.getRelatedTranslation().getRelatedText().getId()) > 1) {
			// 2 times marked as spam -> inform the callback about the spam
			System.out.println("We found spam!! " + em.find(TextToTranslate.class, review.getRelatedTranslation().getRelatedText().getId()));
		} if (review.getReviewComment() == null && !review.isMarkedAsSkipped() && ! review.isMarkedAsSpam() && translationDataAccess.getGoodReviewCount(review.getRelatedTranslation().getId()) > 1) {
			// 2 times correct review -> this translation gets accepted
			TextToTranslate text = em.find(TextToTranslate.class, review.getRelatedTranslation().getRelatedText().getId());
			text.setChoosenTranslation(review.getRelatedTranslation());
			em.merge(text);
			em.flush();
			System.out.println("Sucessfully translated!! " + text + " to " + review.getRelatedTranslation().getTranslatedText());
		}
	}

}
