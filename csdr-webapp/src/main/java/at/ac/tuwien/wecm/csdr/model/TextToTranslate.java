package at.ac.tuwien.wecm.csdr.model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class TextToTranslate extends BaseEntity {

	private static final long serialVersionUID = 1L;
	private UUID externalId;
	private String callbackUrl;
	private String originalText;
	private String originalLanguage;
	private String wantedLanguage;
	private String choosenLanguage;
	private Translation choosenTranslation;
	private List<Translation> translations = new ArrayList<Translation>(0);
	
	public TextToTranslate() {
	}
	public UUID getExternalId() {
		return externalId;
	}
	public void setExternalId(UUID externalId) {
		this.externalId = externalId;
	}
	public String getCallbackUrl() {
		return callbackUrl;
	}
	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}
	@NotNull
	@Size(min = 1)
	public String getOriginalText() {
		return originalText;
	}
	public void setOriginalText(String originalText) {
		this.originalText = originalText;
	}
	@NotNull
	@Size(min = 1)
	public String getOriginalLanguage() {
		return originalLanguage;
	}
	public void setOriginalLanguage(String originalLanguage) {
		this.originalLanguage = originalLanguage;
	}
	@NotNull
	@Size(min = 1)
	public String getWantedLanguage() {
		return wantedLanguage;
	}
	public void setWantedLanguage(String wantedLanguage) {
		this.wantedLanguage = wantedLanguage;
	}
	@NotNull
	@Size(min = 1)
	public String getChoosenLanguage() {
		return choosenLanguage;
	}
	public void setChoosenLanguage(String choosenLanguage) {
		this.choosenLanguage = choosenLanguage;
	}
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	public Translation getChoosenTranslation() {
		return choosenTranslation;
	}
	public void setChoosenTranslation(Translation choosenTranslation) {
		this.choosenTranslation = choosenTranslation;
	}
	@OneToMany(mappedBy = "relatedText", fetch = FetchType.LAZY)
	public List<Translation> getTranslations() {
		return translations;
	}
	public void setTranslations(List<Translation> translations) {
		this.translations = translations;
	}
	@Override
	public String toString() {
		return "TextToTranslate [originalText=" + originalText + ", getId()="
				+ getId() + "]";
	}

}
