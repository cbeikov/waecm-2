package at.ac.tuwien.wecm.csdr.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class TranslationReview extends BaseEntity {

	private static final long serialVersionUID = 1L;

	private String user;
	private String correctedLanguage;
	private String reviewComment;
	private boolean markedAsSpam = false;
	private boolean markedAsSkipped = false;
	private Translation relatedTranslation;

	@NotNull
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getCorrectedLanguage() {
		return correctedLanguage;
	}
	public void setCorrectedLanguage(String correctedLanguage) {
		this.correctedLanguage = correctedLanguage;
	}
	public String getReviewComment() {
		return reviewComment;
	}
	public void setReviewComment(String reviewComment) {
		this.reviewComment = reviewComment;
	}
	public boolean isMarkedAsSpam() {
		return markedAsSpam;
	}
	public void setMarkedAsSpam(boolean markedAsSpam) {
		this.markedAsSpam = markedAsSpam;
	}
	public boolean isMarkedAsSkipped() {
		return markedAsSkipped;
	}
	public void setMarkedAsSkipped(boolean markedAsSkipped) {
		this.markedAsSkipped = markedAsSkipped;
	}
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	public Translation getRelatedTranslation() {
		return relatedTranslation;
	}
	public void setRelatedTranslation(Translation relatedTranslation) {
		this.relatedTranslation = relatedTranslation;
	}
}
