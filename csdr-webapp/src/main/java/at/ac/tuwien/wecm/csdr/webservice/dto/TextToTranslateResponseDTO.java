package at.ac.tuwien.wecm.csdr.webservice.dto;

import java.io.Serializable;
import java.util.UUID;

public class TextToTranslateResponseDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private UUID id;
	
	public TextToTranslateResponseDTO(UUID id) {
		this.id = id;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}
