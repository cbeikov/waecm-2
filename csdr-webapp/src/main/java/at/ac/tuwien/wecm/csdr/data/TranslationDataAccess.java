package at.ac.tuwien.wecm.csdr.data;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import at.ac.tuwien.wecm.csdr.model.TextToTranslate;
import at.ac.tuwien.wecm.csdr.model.Translation;

@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class TranslationDataAccess {
	
	@PersistenceContext
	private EntityManager em;
	
	public long getSpamCount(Long textToTranslateId) {
		return em.createQuery("SELECT COUNT(review) + "
				+ "(SELECT COUNT(t) FROM Translation t WHERE t.markedAsSpam = true AND t.relatedText = text) "
				+ "FROM TextToTranslate text "
				+ "LEFT JOIN text.translations translation "
				+ "LEFT JOIN translation.translationReviews review "
				+ "WHERE text.id = :id "
				+ "AND review.markedAsSpam = true", Long.class)
				.setParameter("id", textToTranslateId)
				.getSingleResult();
	}

	public long getBadReviewCount(Long translationId) {
		return em.createQuery("SELECT COUNT(review) FROM Translation translation "
				+ "LEFT JOIN translation.translationReviews review "
				+ "WHERE translation.id = :id "
				+ "AND review.reviewComment IS NOT NULL", Long.class)
				.setParameter("id", translationId)
				.getSingleResult();
	}

	public long getGoodReviewCount(Long translationId) {
		return em.createQuery("SELECT COUNT(review) FROM Translation translation "
				+ "LEFT JOIN translation.translationReviews review "
				+ "WHERE translation.id = :id "
				+ "AND review.reviewComment IS NULL", Long.class)
				.setParameter("id", translationId)
				.getSingleResult();
	}

	public TextToTranslate getNextText(String user, String fromLanguage, String toLanguage) {
		List<TextToTranslate> results = em.createQuery(
				"FROM TextToTranslate text "
				+ "WHERE text.choosenLanguage = :fromLanguage "
				+ "AND text.wantedLanguage = :toLanguage "
				+ "AND text.choosenTranslation IS NULL "
				+ "AND 2 > (SELECT COUNT(translation) FROM Translation translation WHERE translation.relatedText = text AND translation.markedAsSkipped = true) "
				+ "AND 1 > (SELECT COUNT(translation) FROM Translation translation WHERE translation.relatedText = text AND translation.markedAsSkipped = false AND translation.markedAsWrong = false AND translation.markedAsSpam = false) "
				+ "AND :user NOT IN(SELECT translation.user FROM Translation translation WHERE translation.relatedText = text)", TextToTranslate.class)
				.setParameter("user", user)
				.setParameter("fromLanguage", fromLanguage)
				.setParameter("toLanguage", toLanguage)
				.setFirstResult(0)
				.setMaxResults(1)
				.getResultList();
		
		if (results.isEmpty()) {
			return null;
		}
		
		return results.get(0);
	}

	public Translation getNextTranslation(String user, String fromLanguage, String toLanguage) {
		List<Translation> results = em.createQuery(
				"FROM Translation translation "
				+ "JOIN FETCH translation.relatedText text "
				+ "WHERE ((text.choosenLanguage = :fromLanguage AND translation.correctedLanguage IS NULL) "
				+ "OR (translation.correctedLanguage = :fromLanguage))"
				+ "AND text.wantedLanguage = :toLanguage "
				+ "AND text.choosenTranslation IS NULL "
				+ "AND translation.user != :user "
				+ "AND translation.markedAsSpam = false "
				+ "AND translation.markedAsSkipped = false "
				+ "AND 2 > (SELECT COUNT(review) FROM TranslationReview review WHERE review.relatedTranslation = translation AND review.markedAsSkipped = false) "
				+ "AND :user NOT IN(SELECT review.user FROM TranslationReview review WHERE review.relatedTranslation = translation)", Translation.class)
				.setParameter("user", user)
				.setParameter("fromLanguage", fromLanguage)
				.setParameter("toLanguage", toLanguage)
				.setFirstResult(0)
				.setMaxResults(1)
				.getResultList();
		
		if (results.isEmpty()) {
			return null;
		}
		
		return results.get(0);
	}
}
