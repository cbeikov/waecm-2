package at.ac.tuwien.wecm.csdr.bean;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import at.ac.tuwien.wecm.csdr.data.TranslationDataAccess;
import at.ac.tuwien.wecm.csdr.model.TextToTranslate;
import at.ac.tuwien.wecm.csdr.model.Translation;
import at.ac.tuwien.wecm.csdr.service.TranslationService;

/**
 * 
 * controller for Translation
 */
@Named
@SessionScoped
public class TranslationBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private TranslationDataAccess translationDataAccess;
	@Inject
	private TranslationService translationService;
	@Inject
	private UserBean userBean;
	
	private String fromLanguage;
	private String toLanguage;
	
	private TextToTranslate text;
	private String originalText;
	private String correctedLanguage;
	private String translatedText;
	
	public String startTranslation(String fromLanguage, String toLanguage) {
		this.fromLanguage = fromLanguage;
		this.toLanguage = toLanguage;

		return nextText();
	}
	
	/**
	 * marks text as Spam and returns next text
	 * @return
	 */
	public String spamTranslation() {
		Translation translation = new Translation();
		translation.setMarkedAsSpam(true);
		translation.setRelatedText(text);
		translation.setUser(userBean.getUsername());
		translationService.addTranslation(translation);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Text wurde als Spam markiert!", ""));
		return nextText();
	}
	
	/**
	 * saves current translation and gets next text
	 * @return
	 */
	public String saveTranslation() {
		Translation translation = new Translation();
		
		if (!fromLanguage.equals(correctedLanguage)) {
			translation.setCorrectedLanguage(correctedLanguage);
		}
		
		translation.setRelatedText(text);
		translation.setTranslatedText(translatedText);
		translation.setUser(userBean.getUsername());
		translationService.addTranslation(translation);
		return nextText();
	}

	/**
	 * skips the current text and gets next text
	 * @return
	 */
	public String skipTranslation() {
		Translation translation = new Translation();
		
		if (!fromLanguage.equals(correctedLanguage)) {
			translation.setCorrectedLanguage(correctedLanguage);
		}
		
		translation.setMarkedAsSkipped(true);
		translation.setRelatedText(text);
		translation.setUser(userBean.getUsername());
		translationService.addTranslation(translation);
		return nextText();
	}
	
	private String nextText() {
		text = translationDataAccess.getNextText(userBean.getUsername(), fromLanguage, toLanguage);
		
		if (text == null) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Momentan sind keine Texte für die gewählten Sprachen verfügbar!", ""));
			return "translation.xhtml";
		} else {
			originalText = text.getOriginalText();
			correctedLanguage = fromLanguage;
			translatedText = null;
			return "doTranslate.xhtml";
		}
	}
	
	public String getFromLanguage() {
		return fromLanguage;
	}
	public void setFromLanguage(String fromLanguage) {
		this.fromLanguage = fromLanguage;
	}
	public String getToLanguage() {
		return toLanguage;
	}
	public void setToLanguage(String toLanguage) {
		this.toLanguage = toLanguage;
	}
	public String getCorrectedLanguage() {
		return correctedLanguage;
	}
	public void setCorrectedLanguage(String correctedLanguage) {
		this.correctedLanguage = correctedLanguage;
	}
	public String getOriginalText() {
		return originalText;
	}
	public void setOriginalText(String originalText) {
		this.originalText = originalText;
	}
	@NotNull
	@Size(min = 1)
	public String getTranslatedText() {
		return translatedText;
	}
	public void setTranslatedText(String translatedText) {
		this.translatedText = translatedText;
	}
}
