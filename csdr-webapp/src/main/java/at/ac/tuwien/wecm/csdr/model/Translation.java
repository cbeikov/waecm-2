package at.ac.tuwien.wecm.csdr.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class Translation extends BaseEntity {

	private static final long serialVersionUID = 1L;
	
	private String user;
	private String correctedLanguage;
	private String translatedText;
	private boolean markedAsSpam = false;
	private boolean markedAsSkipped = false;
	private boolean markedAsWrong = false;
	private TextToTranslate relatedText;
	private List<TranslationReview> translationReviews = new ArrayList<TranslationReview>(0);

	@NotNull
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getCorrectedLanguage() {
		return correctedLanguage;
	}
	public void setCorrectedLanguage(String correctedLanguage) {
		this.correctedLanguage = correctedLanguage;
	}
	public String getTranslatedText() {
		return translatedText;
	}
	public void setTranslatedText(String translatedText) {
		this.translatedText = translatedText;
	}
	public boolean isMarkedAsSpam() {
		return markedAsSpam;
	}
	public void setMarkedAsSpam(boolean markedAsSpam) {
		this.markedAsSpam = markedAsSpam;
	}
	public boolean isMarkedAsSkipped() {
		return markedAsSkipped;
	}
	public void setMarkedAsSkipped(boolean markedAsSkipped) {
		this.markedAsSkipped = markedAsSkipped;
	}
	public boolean isMarkedAsWrong() {
		return markedAsWrong;
	}
	public void setMarkedAsWrong(boolean markedAsWrong) {
		this.markedAsWrong = markedAsWrong;
	}
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	public TextToTranslate getRelatedText() {
		return relatedText;
	}
	public void setRelatedText(TextToTranslate relatedText) {
		this.relatedText = relatedText;
	}
	@OneToMany(mappedBy = "relatedTranslation", fetch = FetchType.LAZY)
	public List<TranslationReview> getTranslationReviews() {
		return translationReviews;
	}
	public void setTranslationReviews(List<TranslationReview> translationReviews) {
		this.translationReviews = translationReviews;
	}
}
