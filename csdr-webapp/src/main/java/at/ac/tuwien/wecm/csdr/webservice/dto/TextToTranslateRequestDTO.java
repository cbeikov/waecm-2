package at.ac.tuwien.wecm.csdr.webservice.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import at.ac.tuwien.wecm.csdr.model.TextToTranslate;

public class TextToTranslateRequestDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String callbackUrl;
	private String text;
	private String language;
	private String wantedLanguage;

	public TextToTranslate toModel() {
		TextToTranslate model = new TextToTranslate();
		model.setCallbackUrl(callbackUrl);
		model.setOriginalText(text);
		model.setOriginalLanguage(language);
		model.setChoosenLanguage(language);
		model.setWantedLanguage(wantedLanguage);
		return model;
	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	@NotNull
	@Size(min = 1)
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@NotNull
	@Size(min = 1)
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	@NotNull
	@Size(min = 1)
	public String getWantedLanguage() {
		return wantedLanguage;
	}

	public void setWantedLanguage(String wantedLanguage) {
		this.wantedLanguage = wantedLanguage;
	}

}
