package at.ac.tuwien.wecm.csdr.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.faces.model.SelectItem;
import javax.inject.Named;

/**
 * Controller for supported languages and methods for accessing them
 */
@Named
@RequestScoped
public class LanguagesBean {
	
	private static final String[] LANGUAGES = {
		"Deutsch", "Englisch", "Chinesisch", "Spanisch", "Hindi/Urdu", "Portugiesisch", "Arabisch", "Bengali", "Russisch", "Französisch"
	};
	
	private final Map<String, List<SelectItem>> languagesExcept = new HashMap<String, List<SelectItem>>(0);
	
	private List<SelectItem> languages;
	private List<SelectItem> languagesForCorrection;
	
	/**
	 * @param language
	 * @return  all languages except the language specified in the parameter
	 */
	public List<SelectItem> getLanguagesExcept(String language) {
		if (language == null) {
			return getLanguages();
		}
		
		List<SelectItem> result = languagesExcept.get(language);
		
		if (result != null) {
			return result;
		}
		
		result = new ArrayList<SelectItem>(LANGUAGES.length);
		result.add(new SelectItem(null, "Sprache"));
		
		for (int i = 0; i < LANGUAGES.length; i++) {
			if (!language.equals(LANGUAGES[i])) {
				result.add(new SelectItem(LANGUAGES[i], LANGUAGES[i]));
			}
		}
		
		languagesExcept.put(language, result);
		return result;
	}

	/**
	 * @return all supported languages
	 */
	public List<SelectItem> getLanguages() {
		if (languages != null) {
			return languages;
		}
		
		languages = new ArrayList<SelectItem>(LANGUAGES.length + 1);
		languages.add(new SelectItem(null, "Sprache"));
		
		for (int i = 0; i < LANGUAGES.length; i++) {
			languages.add(new SelectItem(LANGUAGES[i], LANGUAGES[i]));
		}
		
		return languages;
	}
	/**
	 * @return language list for selection to correct the current language
	 */
	public List<SelectItem> getLanguagesForCorrection() {
		if (languagesForCorrection != null) {
			return languagesForCorrection;
		}
		
		languagesForCorrection = new ArrayList<SelectItem>(LANGUAGES.length + 1);
		languagesForCorrection.add(new SelectItem(null, "Unbekannt"));
		
		for (int i = 0; i < LANGUAGES.length; i++) {
			languagesForCorrection.add(new SelectItem(LANGUAGES[i], LANGUAGES[i]));
		}
		
		return languagesForCorrection;
	}
}
