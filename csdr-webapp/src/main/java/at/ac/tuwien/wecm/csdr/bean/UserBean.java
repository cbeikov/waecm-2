package at.ac.tuwien.wecm.csdr.bean;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Bean for prototypically controlling user sessions
 */
@Named
@SessionScoped
public class UserBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String[] USERNAMES = {
		"Karl", "Sepp", "Franz", "Susi", "Mario", "Michi"
	};
	
	private String username;
	
	@PostConstruct
	private void init() {
		this.username = USERNAMES[0];
	}
	
	/**
	 * logs in specified user
	 * @param username
	 * @return
	 */
	public String login(String username) {
		this.username = username;
		return "index.xhtml?faces-redirect=true";
	}
	
	/**
	 * invalidates the session of the current user
	 * @return
	 */
	public String logout() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
		HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
		
		/* disable caching for IE */
		response.setHeader("Cache-Control", "no-cache, no-store");
		response.setHeader("Pragma", "no-cache");

		/* To really remove all user specific data */
		request.getSession().invalidate();
		/* To do not get an error on the next page */
		request.getSession(true);

		return "index.xhtml?faces-redirect=true";
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String[] getUsernames() {
		return USERNAMES;
	}
}
