package at.ac.tuwien.wecm.csdr.webservice;

import java.util.UUID;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import at.ac.tuwien.wecm.csdr.model.TextToTranslate;
import at.ac.tuwien.wecm.csdr.service.TranslationService;
import at.ac.tuwien.wecm.csdr.webservice.dto.TextToTranslateRequestDTO;
import at.ac.tuwien.wecm.csdr.webservice.dto.TextToTranslateResponseDTO;

@Stateless
@Path("/translation")
public class TranslationWebservice {
	
	@Inject
	private TranslationService translationService;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public TextToTranslateResponseDTO translate(TextToTranslateRequestDTO requestDto) {
		TextToTranslate text = requestDto.toModel();
		UUID id = translationService.addText(text);
		TextToTranslateResponseDTO responseDto = new TextToTranslateResponseDTO(id);
		return responseDto;
	}
}
