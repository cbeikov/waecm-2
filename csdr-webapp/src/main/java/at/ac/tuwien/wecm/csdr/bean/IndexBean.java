package at.ac.tuwien.wecm.csdr.bean;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import at.ac.tuwien.wecm.csdr.service.TranslationService;
import at.ac.tuwien.wecm.csdr.webservice.dto.TextToTranslateRequestDTO;

/**
 * Indexbean - controller for the debug view to add texts for translation (which is normally added via web service)
 *
 */
@Named
@ViewScoped
public class IndexBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private TranslationService translationService;
	
	private TextToTranslateRequestDTO dto = new TextToTranslateRequestDTO();
	
	public void add() {
		translationService.addText(dto.toModel());
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Erfolgreich erstellt!", ""));
	}

	public TextToTranslateRequestDTO getDto() {
		return dto;
	}

	public void setDto(TextToTranslateRequestDTO dto) {
		this.dto = dto;
	}
	
	
}
